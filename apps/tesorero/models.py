# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from apps.recaudador.models import *
from django.contrib.auth.models import User, Permission, Group
from apps.tesorero import constants

# Create your models here.
class Balance_trimestre(models.Model):
	trimestre = models.CharField(max_length = 100, blank =True, null=True)
	ano = models.PositiveIntegerField(blank=True, null=True)
	total_recaudado = models.PositiveIntegerField(blank=True, null=True)
	comision = models.PositiveIntegerField(blank=True, null=True)
	total_compania = models.PositiveIntegerField(blank=True, null=True)

	def __str__(self):
		return str(self.id)

class Meses_trimestre(models.Model):
	balance = models.ForeignKey('Balance_trimestre', on_delete =  models.CASCADE, null= True, blank=True)
	sector = models.ForeignKey('recaudador.Sector', on_delete=models.CASCADE, null=True,blank=True)
	recaudador = models.ForeignKey('recaudador.Profile', on_delete=models.CASCADE, null=True,blank=True)
	mes = models.PositiveIntegerField(blank=True, null=True, choices = constants.MESES_CHOICES)
	ano = models.PositiveIntegerField(blank=True, null=True)
	total_recaudado = models.PositiveIntegerField(blank=True, null=True)
	comision = models.PositiveIntegerField(blank=True, null=True)
	total_compania = models.PositiveIntegerField(blank=True, null=True)

	def __str__(self):
		return str(self.balance.trimestre)
