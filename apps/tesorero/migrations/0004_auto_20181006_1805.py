# Generated by Django 2.1 on 2018-10-06 18:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tesorero', '0003_meses_trimestre_recaudador'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meses_trimestre',
            name='mes',
            field=models.PositiveIntegerField(blank=True, choices=[('1', 'Enero'), ('2', 'Febrero'), ('3', 'Marzo'), ('4', 'Abril'), ('5', 'Mayo'), ('6', 'Junio'), ('7', 'Julio'), ('8', 'Agosto'), ('9', 'Septiembre'), ('10', 'Octubre'), ('11', 'Noviembre'), ('12', 'Diciembre')], null=True),
        ),
        migrations.AlterField(
            model_name='meses_trimestre',
            name='recaudador',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='recaudador.Profile'),
        ),
    ]
