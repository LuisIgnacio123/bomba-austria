# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.tesorero.models import *
# Register your models here.


class Meses_trimestreInLine(admin.TabularInline):
	model = Meses_trimestre
	extra = 3

@admin.register(Balance_trimestre)
class Balance_trimestreAdmin(admin.ModelAdmin):
	list_display = ('trimestre','total_recaudado', 'comision', 'total_compania',)
	search_fields = ['id','trimestre']
	inlines = (Meses_trimestreInLine,)