from django.urls import path
from apps.tesorero import views
app_name = "tesorero"

urlpatterns = [
	path('', views.tesorero, name='tesorero'),
	path('visualizar_balance/', views.visualizar_balance, name='visualizar_balance'),
	path('crear_balance/', views.crear_balance, name='crear_balance'),
	path('visualizar_socios_activos/', views.visualizar_socios_activos, name='visualizar_socios_activos'),
	path('visualizar_boletas_anuladas/', views.visualizar_boletas_anuladas, name='visualizar_boletas_anuladas'),
	path('editar_boletas_anuladas/<int:id_boleta>/', views.editar_boletas_anuladas, name='editar_boletas_anuladas'),
	path('visualizar_recaudador/', views.visualizar_recaudador, name='visualizar_recaudador'),
	path('guardar_boleta/',views.guardar_boleta, name='guardar_boleta'),
	path('visualizar_detalle_blance/',views.visualizar_detalle_blance, name='visualizar_detalle_blance'),
	path('graficar/',views.graficar, name='graficar'),
	path('visualizar_recaudador/', views.visualizar_recaudador, name='visualizar_recaudador'),
	path('modificar_recaudador/<int:id_recaudador>/', views.modificar_recaudador, name='modificar_recaudador'),
	path('guardar_modificado/', views.guardar_modificado, name='guardar_modificado'),
	path('cargar_datos/<int:trimestre>', views.cargar_datos, name='cargar_datos'),
	path('guardar_balance/', views.guardar_balance, name='guardar_balance'),
	path('modificar_balance/<int:id_balance>', views.modificar_balance, name='modificar_balance'),
	path('guardar_balance_editado/', views.guardar_balance_editado, name='guardar_balance_editado'),
	path('modificar_socio/<int:id_socio>/', views.modificar_socio, name='modificar_socio'),
]