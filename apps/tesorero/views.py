# -*- coding: utf-8 -*-
import datetime
# from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core import serializers
from django.contrib.auth.models import User, Group
from apps.recaudador.models import Boleta, Socio, Profile
from apps.tesorero.models import *
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Sum

def group_check(user):
	groups = Group.objects.get(user = user)
	if groups.name == 'Tesorero' or user.is_superuser:
		return True
	else:
		return False

@login_required()
def tesorero (request):
	context = {}
	if group_check(request.user):
		template_name = "tesorero/inicio.html"
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def graficar (request):
	boletas = Boleta.objects.filter(boleta_estado = True).order_by('boleta_fecha')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	datos = {}
	for boleta in boletas:
		if boleta.boleta_aporte != None and boleta.boleta_fecha != None:
			fecha_boleta = str(boleta.boleta_fecha)
			fecha_separada_boleta = fecha_boleta.split('-')
			if fecha_separada_boleta[0] not in datos.keys():
				datos[fecha_separada_boleta[0]] = boleta.boleta_aporte
			elif fecha_separada_boleta[0] in datos.keys():
				datos[fecha_separada_boleta[0]] += boleta.boleta_aporte
	return JsonResponse(datos)

@login_required()
def visualizar_balance(request):
	if group_check(request.user):
		template_name = "tesorero/visualizar_balance_trimestral.html"
		context = {}
		context['balances'] = Balance_trimestre.objects.all()
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def crear_balance(request):
	mensaje = ""
	recaudadores = Profile.objects.all()
	trimestres = {'1': [1,2,3], '2':[4,5,6], '3':[7,8,9], '4':[10,11,12]}
	boletas = Boleta.objects.filter(boleta_estado = True).order_by('boleta_fecha')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	bal = Balance_trimestre()
	balance = {}
	totales = {}

	for boleta in boletas:
		if boleta.boleta_aporte != None and boleta.boleta_fecha != None:
			fecha_boleta = str(boleta.boleta_fecha)
			fecha_separada_boleta = fecha_boleta.split('-')
			if fecha_separada_boleta[0] == año:
				for key in trimestres:
					if int(mes) in trimestres[key] and int(fecha_separada_boleta[1]) in trimestres[key]:
						if fecha_separada_boleta[1] not in balance.keys():
							balance[fecha_separada_boleta[1]] = []
							balance[fecha_separada_boleta[1]].append(boleta)
						elif fecha_separada_boleta[1] in balance.keys():
							balance[fecha_separada_boleta[1]].append(boleta)
	for mes in balance:
		for recaudador in recaudadores:
			for boletas in balance[mes]:
				if boletas.boleta_recaudador == recaudador:
					if recaudador not in totales.keys():
						totales[recaudador] = boletas.boleta_aporte
					elif recaudador in totales.keys():
						totales[recaudador] += boletas.boleta_aporte

	return HttpResponse(JsonResponse({'respuesta': mensaje}), content_type='applicaction/json')


def visualizar_detalle_blance(request, id_balance):
	if group_check(request.user):
		template_name = "tesorero/visualizar_balance.html"
		context = {}
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def visualizar_socios_activos(request):
	if group_check(request.user):
		template_name = "tesorero/visualizar_socios_activos.html"
		context = {}
		context['Socios'] = Socio.objects.filter(socio_estado = 'Activo')
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def modificar_socio(request, id_socio):
	if group_check(request.user):
		template_name = "tesorero/modificar_socio.html"
		context = {}
		context['socio'] = Socio.objects.get(id = id_socio)
		context['sectores'] = Sector.objects.all()
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)


@login_required()
def visualizar_boletas_anuladas(request):
	if group_check(request.user):
		template_name = "tesorero/visualizar_boletas_anuladas.html"
		context = {}
		context['boletas'] = Boleta.objects.filter(boleta_estado = False)	
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def editar_boletas_anuladas(request, id_boleta):
	if group_check(request.user):
		template_name = "tesorero/editar_boletas_anuladas.html"
		context = {}
		context['boleta'] = Boleta.objects.get(boleta_numero = id_boleta)
		fecha = str(context['boleta'].boleta_fecha)
		fecha_separada = fecha.split('-')
		context['fecha'] = fecha_separada[0] + "-" + fecha_separada[1] + "-" + fecha_separada[2]
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id)
		context['recaudadores'] = recaudadores
		context['socios'] = Socio.objects.all().order_by('socio_nombre')
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_boleta(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	recaudador = 0
	for data in ddatos_json:
		if 'recaudador' in data.keys():
			recaudador = int(data['recaudador'])
		if 'Boleta' in data.keys():
			boleta = data['Boleta']
		if 'Talonario' in data.keys():
			talonario = data['Talonario']
		if 'Socio' in data.keys():
			socio = int(data['Socio'])
		if 'boleta_fecha' in data.keys():
			Boleta_fecha = data['boleta_fecha']
		if 'Aporte' in data.keys():
			aporte = data['Aporte']
		if 'Observacion' in data.keys():
			observacion = data['Observacion']
		if 'estado' in data.keys():
			Estado = data['estado']
	if Boleta.objects.get(boleta_numero = boleta):
		try:
			rec = Profile.objects.get(user = recaudador)
		except:
			rec = Profile.objects.get(id = recaudador)
		soc =  Socio.objects.get(id = socio)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_recaudador = rec)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_talonario = talonario)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_socio = soc)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_fecha = Boleta_fecha)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_estado = Estado)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_socio_nombre = soc.socio_nombre)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_socio_direccion = soc.socio_direccion)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_socio_sector = soc.socio_sector)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_aporte = aporte)
		Boleta.objects.filter(boleta_numero = boleta).update(boleta_socio_observacion = observacion)
		mensaje = "correcto"
	else:
		mensaje = "incorrecto"

	return HttpResponse(JsonResponse({'respuesta': mensaje}), content_type='applicaction/json')

@login_required()
def visualizar_recaudador(request):
	if group_check(request.user):
		template_name = "tesorero/visualizar_recaudador.html"
		context = {}
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo)
		context['recaudadores'] = recaudadores
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def modificar_recaudador(request, id_recaudador):
	if group_check(request.user):
		template_name = "tesorero/modificar_recaudador.html"
		context = {}
		recaudador = User.objects.get(id = id_recaudador)
		context['recaudador'] = recaudador
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_modificado(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'Nombre_user' in data.keys():
			usuario = data['Nombre_user']
		if 'Nombre' in data.keys():
			nombre = data['Nombre']
		if 'correo' in data.keys():
			correo = data['correo']
		if 'password' in data.keys():
			password = data['password']
		if 'estado' in data.keys():
			estado = data['estado']

	user = User.objects.get(username = usuario)
	full_name = nombre.split(" ")
	nombre1 = full_name[0]
	if len(full_name) >= 3:
		apellidos = full_name[1] + " " + full_name[2]
	if len(full_name) == 2:
		apellidos = full_name[1]
	if len(full_name) == 1:
		apellidos = ""
	if estado != user.is_active and password != "":
		User.objects.filter(id = user.id).update(first_name = nombre1)
		User.objects.filter(id = user.id).update(last_name = apellidos)
		User.objects.filter(id = user.id).update(email = correo)
		User.objects.filter(id = user.id).update(is_active = estado)
		user.set_password(password)
		user.save()
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	if estado != user.is_active and password == "":
		User.objects.filter(id = user.id).update(first_name = nombre1)
		User.objects.filter(id = user.id).update(last_name = apellidos)
		User.objects.filter(id = user.id).update(email = correo)
		User.objects.filter(id = user.id).update(is_active = estado)
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	else:
		return HttpResponse(JsonResponse({"respuesta" : "incorrecto"}), content_type='applicaction/json')

@login_required()
def crear_balance(request):
	if group_check(request.user):
		template_name = "tesorero/crear_balance.html"
		context = {}
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def cargar_datos(request, trimestre):
	datos_enviar = []
	fecha_actual = datetime.datetime.now()

	if trimestre == 1:
		# TRIMESTRE 1
		inicio_primertrimestre = datetime.datetime(int(fecha_actual.year), 1, 1)
		final_primertrimestre = datetime.datetime(int(fecha_actual.year), 3, 31)
		primer_trimestre = Boleta.objects.filter(boleta_fecha__range=(inicio_primertrimestre, final_primertrimestre))
		sectores = Sector.objects.all()
		grupo = Group.objects.get(name = "Recaudador")
		recaudadores = User.objects.filter(groups = grupo)
		if primer_trimestre:
			for mes in range(1,4):
				for sector in sectores:
					for recaudador in recaudadores:
						aux = {}
						if mes == 1:
							aux['pos'] = 1
						elif mes == 2:
							aux['pos'] = 2
						elif mes == 3:
							aux['pos'] = 3
						aux['mes'] = mes
						aux['sector'] = sector.sector_nombre
						aux['id_sector'] = sector.id
						aux['recaudador'] = recaudador.get_full_name()
						aux['id_recaudador'] = recaudador.id
						total = Boleta.objects.filter(boleta_fecha__month = mes, boleta_fecha__year = int(fecha_actual.year)).filter(boleta_socio_sector = sector).aggregate(Sum('boleta_aporte'))['boleta_aporte__sum']
						if total != None:
							aux['total'] = total
							datos_enviar.append(aux)
						else:
							continue
		sectores_agregados = []
		for recaudador in recaudadores:
			for sector in sectores:
				if sector not in sectores_agregados:
					aux2 = {}
					suma_total = primer_trimestre.filter(boleta_socio_sector = sector).filter(boleta_recaudador__user = recaudador).aggregate(
								Sum('boleta_aporte'))['boleta_aporte__sum']
					if suma_total != None:
						aux2['pos'] = 4
						aux2['trimestre'] = "Primer Trimestre"
						aux2['sector'] = sector.sector_nombre
						aux2['recaudador'] = recaudador.get_full_name()
						aux2['total'] = suma_total
						datos_enviar.append(aux2)
						sectores_agregados.append(sector)
					else:
						continue

	elif trimestre == 2:
		# TRIMESTRE 2
		inicio_segundotrimestre = datetime.datetime(int(fecha_actual.year), 4, 1)
		final_segundotrimestre = datetime.datetime(int(fecha_actual.year), 6, 30)
		segundo_trimestre = Boleta.objects.filter(boleta_fecha__range=(inicio_segundotrimestre, final_segundotrimestre))
		sectores = Sector.objects.all()
		grupo = Group.objects.get(name = "Recaudador")
		recaudadores = User.objects.filter(groups = grupo)
		if segundo_trimestre:
			for mes in range(4,7):
				for sector in sectores:
					for recaudador in recaudadores:
						aux = {}
						if mes == 4:
							aux['pos'] = 1
						elif mes == 5:
							aux['pos'] = 2
						elif mes == 6:
							aux['pos'] = 3
						aux['mes'] = mes
						aux['sector'] = sector.sector_nombre
						aux['id_sector'] = sector.id
						aux['recaudador'] = recaudador.get_full_name()
						aux['id_recaudador'] = recaudador.id
						total = Boleta.objects.filter(boleta_fecha__month = mes, boleta_fecha__year = int(fecha_actual.year)).filter(boleta_socio_sector = sector).aggregate(Sum('boleta_aporte'))['boleta_aporte__sum']
						if total != None:
							aux['total'] = total
							datos_enviar.append(aux)
						else:
							continue
		sectores_agregados = []
		for recaudador in recaudadores:
			for sector in sectores:
				if sector not in sectores_agregados:
					aux2 = {}
					suma_total = segundo_trimestre.filter(boleta_socio_sector = sector).filter(boleta_recaudador__user = recaudador).aggregate(
								Sum('boleta_aporte'))['boleta_aporte__sum']
					if suma_total != None:
						aux2['pos'] = 4
						aux2['trimestre'] = "Segundo Trimestre"
						aux2['sector'] = sector.sector_nombre
						aux2['recaudador'] = recaudador.get_full_name()
						aux2['total'] = suma_total
						datos_enviar.append(aux2)
						sectores_agregados.append(sector)
					else:
						continue

		

	elif trimestre == 3:
		# TRIMESTRE 3
		inicio_tercertrimestre = datetime.datetime(int(fecha_actual.year), 7, 1)
		final_tercertrimestre = datetime.datetime(int(fecha_actual.year), 9, 30)
		tercer_trimestre = Boleta.objects.filter(boleta_fecha__range=(inicio_tercertrimestre, final_tercertrimestre))
		sectores = Sector.objects.all()
		grupo = Group.objects.get(name = "Recaudador")
		recaudadores = User.objects.filter(groups = grupo)
		if tercer_trimestre:
			for mes in range(7,10):
				for sector in sectores:
					for recaudador in recaudadores:
						aux = {}
						if mes == 7:
							aux['pos'] = 1
						elif mes == 8:
							aux['pos'] = 2
						elif mes == 9:
							aux['pos'] = 3
						aux['mes'] = mes
						aux['sector'] = sector.sector_nombre
						aux['id_sector'] = sector.id
						aux['recaudador'] = recaudador.get_full_name()
						aux['id_recaudador'] = recaudador.id
						total = Boleta.objects.filter(boleta_fecha__month = mes, boleta_fecha__year = int(fecha_actual.year)).filter(boleta_socio_sector = sector).aggregate(Sum('boleta_aporte'))['boleta_aporte__sum']
						if total != None:
							aux['total'] = total
							datos_enviar.append(aux)
						else:
							continue
		sectores_agregados = []
		for recaudador in recaudadores:
			for sector in sectores:
				if sector not in sectores_agregados:
					aux2 = {}
					suma_total = tercer_trimestre.filter(boleta_socio_sector = sector).filter(boleta_recaudador__user = recaudador).aggregate(
								Sum('boleta_aporte'))['boleta_aporte__sum']
					if suma_total != None:
						aux2['pos'] = 4
						aux2['trimestre'] = "Tercer Trimestre"
						aux2['sector'] = sector.sector_nombre
						aux2['recaudador'] = recaudador.get_full_name()
						aux['id_recaudador'] = recaudador.id
						aux2['total'] = suma_total
						datos_enviar.append(aux2)
						sectores_agregados.append(sector)
					else:
						continue
		

	elif trimestre == 4: 
		inicio_cuartotrimestre = datetime.datetime(int(fecha_actual.year), 10, 1)
		final_cuartotrimestre = datetime.datetime(int(fecha_actual.year), 12, 31)
		cuarto_trimestre = Boleta.objects.filter(boleta_fecha__range=(inicio_cuartotrimestre, final_cuartotrimestre))
		sectores = Sector.objects.all()
		boletas_por_recaudador = []
		grupo = Group.objects.get(name = "Recaudador")
		recaudadores = User.objects.filter(groups = grupo)
		for mes in range(10,13):
			for sector in sectores:
				for recaudador in recaudadores:
					aux = {}
					if mes == 10:
						aux['pos'] = 1
					elif mes == 11:
						aux['pos'] = 2
					elif mes == 12:
						aux['pos'] = 3
					aux['mes'] = mes
					aux['sector'] = sector.sector_nombre
					aux['id_sector'] = sector.id
					aux['recaudador'] = recaudador.get_full_name()
					aux['id_recaudador'] = recaudador.id
					total = Boleta.objects.filter(boleta_fecha__month = mes, boleta_fecha__year = int(fecha_actual.year)).filter(boleta_socio_sector = sector).aggregate(Sum('boleta_aporte'))['boleta_aporte__sum']
					if total != None:
						aux['total'] = total
						datos_enviar.append(aux)
					else:
						continue
		sectores_agregados = []
		for recaudador in recaudadores:
			for sector in sectores:
				if sector not in sectores_agregados:
					aux2 = {}
					suma_total = cuarto_trimestre.filter(boleta_socio_sector = sector).filter(boleta_recaudador__user = recaudador).aggregate(
								Sum('boleta_aporte'))['boleta_aporte__sum']
					if suma_total != None:
						aux2['pos'] = 4
						aux2['trimestre'] = "Cuarto Trimestre"
						aux2['sector'] = sector.sector_nombre
						aux2['recaudador'] = recaudador.get_full_name()
						aux2['total'] = suma_total
						datos_enviar.append(aux2)
						sectores_agregados.append(sector)
					else:
						continue
	if len(datos_enviar) == 0:
		return JsonResponse({'mensaje' : "No se encontraron Datos"})
	else:
		return JsonResponse({'context': datos_enviar})

def guardar_balance(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	total_recaudado_gral = 0
	total_compania_gral = 0
	total_comision_gral = 0
	balance = Balance_trimestre()
	fecha_actual = datetime.datetime.now()
	ano = str(fecha_actual.year)
	paso = False

	for data in ddatos_json:
		if 'trimestre' in data.keys():
			total_compania_gral += int(data['total_compania'])
			total_recaudado_gral += int(data['total_recaudado'])
			total_comision_gral += int(data['total_comision'])
			trimestre = data['trimestre'] + ano
	try:
		#Se genera un nuevo balance
		balance.trimestre = trimestre
		balance.ano = ano
		balance.total_recaudado = total_recaudado_gral
		balance.comision = total_comision_gral
		balance.total_compania = total_compania_gral
		#si se guarda se genera los detalles
		balance.save()
		for data in ddatos_json:
			if 'mes' in data.keys():
				meses_trimestre = Meses_trimestre()
				meses_trimestre.balance =  balance
				meses_trimestre.sector = Sector.objects.get(id = int(data['sector']))
				meses_trimestre.recaudador = Profile.objects.get(user = int(data['recaudador']))
				meses_trimestre.mes = data['mes']
				meses_trimestre.ano = ano
				meses_trimestre.total_recaudado = int(data['total_recaudado'])
				meses_trimestre.comision = int(data['total_comision'])
				meses_trimestre.total_compania = int(data['total_compania'])
				meses_trimestre.save()
				paso = True
	except:
		balance.delete()
		paso = False

	if paso == True:
		return HttpResponse(JsonResponse({"respuesta" : "Se guardo Correctamente el balance", "paso" : paso}), content_type='applicaction/json')
	else:
		return HttpResponse(JsonResponse({"respuesta" : "Ocurrio un Error! Comuniquese con el Administrador", "paso" : paso}), content_type='applicaction/json')
@login_required()
def modificar_balance(request,id_balance):
	if group_check(request.user):
		template_name = "tesorero/modificar_balance.html"
		context = {}
		context['totales'] = Balance_trimestre.objects.get(id = id_balance)
		context['meses'] = Meses_trimestre.objects.filter(balance = id_balance)
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Tesorero!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_balance_editado(request):
	pass





