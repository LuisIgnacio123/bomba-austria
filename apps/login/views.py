# from django.shortcuts import render_to_response
from django.template import RequestContext
from .forms import UserForm
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, logout, login
from apps.recaudador.views import inicio
from django.conf import settings

# Create your views here.

def login_auth(request):
	# logout(request)
	context = {}
	if request.POST:
		username = request.POST["username"]
		pwd = request.POST["password"]
		if authenticate(username=username, password=pwd):
			user = authenticate(username=username, password=pwd)
			users = User.objects.get(username = username)
			if Group.objects.filter(user = users):
				groups = Group.objects.get(user = users)
				if user is not None:
					if groups.name == "Recaudador":
						login(request, user)
						return HttpResponseRedirect('/recaudador')
					if groups.name == "Secretario":
						login(request, user)
						return HttpResponseRedirect('/secretario')
					if groups.name == "Tesorero":
						login(request, user)
						return HttpResponseRedirect('/tesorero')
					if groups.name == "administrador":
						login(request, user)
						return HttpResponseRedirect('/administrador')
			elif user.is_staff and user.is_superuser:
				login(request, user)
				return HttpResponseRedirect('/administrador')
		else:
			context['messages'] =  "Usuario o Contraseña Invalidado!"
			return render(request, 'login/login.html', context)
	
	return render(request, 'login/login.html')

def logout_auth(request):
	logout(request)
	return redirect('login_auth')

def register_user(request):
	if request.method == 'POST':
		form = UserForm(request.POST)
		if form.is_valid():
			username, email = form.cleaned_data['username'], form.cleaned_data['email']
			password = form.cleaned_data['password']
			new_user = User.objects.create_user(username, email, password)
			new_user.is_active = True
			new_user.save()
			return render(request, 'login.html')
	else:
		form = UserForm()

	return render(request, 'crear_usuario.html', {'form': form})

# HTTP Error 404
def handler404(request):
	data = {}
	return render(request,'login/error-404.html', data)

def handler500(request):
	data = {}
	return render(request,'login/error-404.html', data)