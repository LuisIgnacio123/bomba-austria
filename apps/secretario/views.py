# -*- coding: utf-8 -*-
import datetime
# from __future__ import unicode_literals
from django.shortcuts import render
from apps.recaudador.models import *
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core import serializers
from django.contrib.auth.models import User, Group
from apps.recaudador.models import Profile, Socio
from django.contrib.auth.decorators import login_required, permission_required

def group_check(user):
	groups = Group.objects.get(user = user)
	if groups.name == 'Secretario' or user.is_superuser:
		return True
	else:
		return False

@login_required()
def secretario (request):
	context = {}
	if group_check(request.user):
		template_name = "secretario/inicio.html"
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)

def graficar (request):
	socios = Socio.objects.order_by('id')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	datos = {}
	
	for socio in socios:
		if socio.socio_inscripcion != None:
			fecha_inscripcion = str(socio.socio_inscripcion)
			fecha_separada_incripcion = fecha_inscripcion.split('-')
			if fecha_separada_incripcion[0] not in datos.keys():
				datos[fecha_separada_incripcion[0]] = 1
			elif fecha_separada_incripcion[0] in datos.keys():
				datos[fecha_separada_incripcion[0]] += 1
	return JsonResponse(datos)

@login_required()
def ingresar_recaudador(request):
	context = {}
	if group_check(request.user):
		template_name = "secretario/guardar_recaudador.html"
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_recaudador(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'Nombre_user' in data.keys():
			nombre_usuario = data['Nombre_user']
		if 'Nombre' in data.keys():
			nombre = data['Nombre']
		if 'Apellido' in data.keys():
			apellido =  data['Apellido']
		if 'email' in data.keys():
			email = data['email']
		if 'password' in data.keys():
			contrasena = data['password']
	if not User.objects.filter(username = nombre_usuario):
		grupo = Group.objects.get(name = 'Recaudador')
		new_user = User.objects.create_user(
			username = nombre_usuario,
			first_name = nombre,
			last_name = apellido,
			email = email,
			password = contrasena)
		new_user.is_active = True
		new_user.is_staff = False
		new_user.is_superuser = False
		new_user.save()
		new_user.groups.add(grupo)
		perfil_recaudador = Profile()
		perfil_recaudador.user = new_user
		perfil_recaudador.save()
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	else:
		usuario_existe = "Nombre usuario ya existe"
		return HttpResponse(JsonResponse({"respuesta":usuario_existe}), content_type='applicaction/json')

@login_required()
def visualizar_socios(request):
	context = {}
	if group_check(request.user):
		template_name = "secretario/visualizar_socios.html"
		socios = Socio.objects.all()
		context['socios'] =  socios
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def modificar_socio(request, id_socio):
	context = {}
	if group_check(request.user):
		template_name = "secretario/modificar_socio.html"
		data = Socio.objects.get(id = id_socio)
		fecha = data.socio_fecha_nacimiento
		context['socio'] = data
		context['fecha'] = fecha
		context['sectores'] = Sector.objects.all()
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_socio(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	socio = 0
	for data in ddatos_json:
		if 'codigo' in data.keys() :
			socio = int(data['codigo'])
		if 'Nombre' in data.keys():
			nombre = data['Nombre']
		if 'Rut' in data.keys():
			rut = data['Rut']
		if 'Direccion' in data.keys():
			direccion = data['Direccion']
		if 'Sector' in data.keys():
			sector = data['Sector']
		if 'F_nac' in data.keys():
			if data['F_nac'] != '':
				nacimiento = data['F_nac']
		if 'Monto' in data.keys():
			monto = data['Monto']
		if 'Pago' in data.keys():
			pago = data['Pago']
		if 'estado' in data.keys():
			Estado = data['estado']
	if Socio.objects.get(id = socio):
		sec = Sector.objects.get(id = sector)
		Socio.objects.filter(id = socio).update(socio_nombre = nombre)
		Socio.objects.filter(id = socio).update(socio_rut = rut)
		Socio.objects.filter(id = socio).update(socio_direccion = direccion)
		Socio.objects.filter(id = socio).update(socio_sector = sector)
		Socio.objects.filter(id = socio).update(socio_aporte = monto)
		Socio.objects.filter(id = socio).update(socio_dia_pago = pago)
		Socio.objects.filter(id = socio).update(socio_estado = Estado)
		return HttpResponse(JsonResponse({"respuesta" : "correcto"}), content_type='applicaction/json')
	else:
		return HttpResponse(JsonResponse({"respuesta" : "incorrecto"}), content_type='applicaction/json')

@login_required()
def visualizar_estado_cuenta(request):
	context = {}
	if group_check(request.user):
		template_name = 'secretario/visualizar_estado_cuenta.html'
		context = {}
		boletas_pagas = {}
		boletas_teoricas = {}
		x = datetime.datetime.now()
		if x.day < 10 and x.month < 10:
			fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + "0" + str(x.day)
		elif x.day < 10 and x.month >= 10:
			fecha_actual = str(x.year) + "-" + str(x.month) + "-" + "0" + str(x.day)
		elif x.day >= 10 and x.month >= 10:
			fecha_actual = str(x.year) + "-" + str(x.month) + "-" + str(x.day)
		elif x.day >= 10 and x.month < 10:
			fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + str(x.day)
		socios = Socio.objects.all()
		try:
			for socio in socios:
				if socio not in boletas_pagas.keys():
					boletas_pagas[socio] = Boleta.objects.filter(boleta_socio = socio.id).filter(boleta_fecha__range = [socio.socio_inscripcion, fecha_actual]).count()
					print(boletas_pagas)
					contador = 0
					inscripcion_ano= int(socio.socio_inscripcion.year)
					inscripcion_mes = int(socio.socio_inscripcion.month)
					while inscripcion_ano <= int(x.year):
						while inscripcion_mes <= int(x.month):
							contador += 1
							inscripcion_mes += 1
						inscripcion_ano += 1
					boletas_teoricas[socio] = contador
			context['boletas'] = boletas_pagas
			context['teorico'] = boletas_teoricas

			return render(request, template_name, context)
		except:
			return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)
	
@login_required()
def visualizar_boletas(request, id_socio):
	context = {}
	if group_check(request.user):
		template_name = 'secretario/visualizar_boletas.html'
		context = {}
		x = datetime.datetime.now()
		if x.day < 10 and x.month < 10:
			fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + "0" + str(x.day)
		elif x.day < 10 and x.month >= 10:
			fecha_actual = str(x.year) + "-" + str(x.month) + "-" + "0" + str(x.day)
		elif x.day >= 10 and x.month >= 10:
			fecha_actual = str(x.year) + "-" + str(x.month) + "-" + str(x.day)
		elif x.day >= 10 and x.month < 10:
			fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + str(x.day)

		boletas = Boleta.objects.filter(boleta_socio = id_socio).filter(boleta_estado = True).filter(boleta_fecha__lte = fecha_actual )
		context['boletas'] = boletas
		print (context)
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Secretario!"
		return render(request, 'login/sin_permisos.html', context)





