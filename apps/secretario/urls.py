from django.urls import path
from apps.secretario import views
app_name = "secretario"

urlpatterns = [
	path('', views.secretario, name='secretario'),
	path('ingresar_recaudador/', views.ingresar_recaudador, name='ingresar_recaudador'),
	path('guardar_recaudador/', views.guardar_recaudador, name='guardar_recaudador'),
	path('visualizar_socios/', views.visualizar_socios, name='visualizar_socios'),
	path('modificar_socio/<int:id_socio>/', views.modificar_socio, name='modificar_socio'),
	path('guardar_socio/', views.guardar_socio, name='guardar_socio'),
	path('graficar/',views.graficar, name='graficar'),
	path('estados_cuentas/', views.visualizar_estado_cuenta, name='estados_cuentas'),
	path('visualizar_boletas/<int:id_socio>/', views.visualizar_boletas, name='visualizar_boletas'),
	
]