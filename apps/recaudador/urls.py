from django.urls import path
from apps.recaudador import views
app_name = "recaudador"

urlpatterns = [
	path('', views.inicio, name='recaudador'),
	path('realizar_pago/', views.realizar_pago, name='realizar_pago'),
	path('guardar_pago/', views.guardar_pago, name='guardar_pago'),
	path('boletas/', views.boletas, name ='boletas'),
	path('buscar_recibo/', views.buscar_recibo, name ='buscar_recibo'),
	path('anular/',views.anular_boleta,name='anular_boleta'),
	path('ingresar_socio/', views.ingresar_socio, name = 'ingresar_socio'),
	path('guardar_socio/',views.guardar_socio,name='guardar_socio'),
	path('buscar_IDsocio/',views.buscar_IDsocio, name='buscar_IDsocio'),
	path('buscar_Dirsocio/',views.buscar_Dirsocio, name='buscar_Dirsocio'),
	path('buscar_Nombresocio/',views.buscar_Nombresocio, name='buscar_Nombresocio'),
	path('graficar/',views.graficar, name='graficar'),
	path('validar_talonario/',views.validar_talonario, name='validar_talonario'),
	path('validar_boleta/',views.validar_boleta, name='validar_boleta'),
]