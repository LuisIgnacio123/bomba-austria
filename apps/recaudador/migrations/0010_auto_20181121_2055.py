# Generated by Django 2.1.1 on 2018-11-21 20:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recaudador', '0009_merge_20181121_2055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boleta',
            name='boleta_estado',
            field=models.BooleanField(blank=True, default=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='pertenece_compania',
            field=models.BooleanField(blank=True, default=True),
        ),
    ]
