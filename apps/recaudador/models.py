# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, Permission, Group
from apps.recaudador import constants
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.

class Boleta(models.Model):
	boleta_numero = models.PositiveIntegerField(blank=True, null=True)
	boleta_talonario = models.ForeignKey('Talonario', on_delete =  models.CASCADE, null= True, blank=True)
	boleta_socio = models.ForeignKey('Socio', on_delete = models.CASCADE, null=True, blank=True)
	boleta_fecha = models.DateField(null=True, blank=True)
	boleta_aporte = models.PositiveIntegerField(null=True, blank=True)
	boleta_estado = models.BooleanField(default=True, blank=True)
	boleta_recaudador = models.ForeignKey('Profile',on_delete = models.CASCADE, null=True,blank=True)
	boleta_socio_nombre = models.CharField(max_length=100, blank=True, null=True)
	boleta_socio_direccion = models.CharField(max_length=99999, blank=True, null=True)
	boleta_socio_sector = models.ForeignKey('Sector',null=True,blank=True, on_delete = models.CASCADE)
	boleta_socio_observacion = models.TextField(blank=True, null=True)


	def __str__(self):
		return str(self.boleta_talonario.id)


class Socio(models.Model):
	socio_nombre = models.CharField(max_length = 100, null=True, blank=True)
	socio_rut = models.CharField(max_length = 11, null=True, blank=True)
	socio_direccion = models.CharField(max_length = 100, null=True, blank=True)
	socio_sector = models.ForeignKey('Sector',null=True,on_delete = models.CASCADE, blank=True)
	socio_fecha_nacimiento = models.DateField(null=True, blank=True)
	socio_telefono = models.CharField(max_length=100, null=True, blank=True)
	socio_aporte = models.PositiveIntegerField(null=True, blank=True)
	socio_dia_pago = models.PositiveIntegerField(validators=[MaxValueValidator(31), MinValueValidator(1)],null=True, blank=True)
	socio_estado = models.CharField(max_length = 100, choices = constants.ESTADO_CHOICES, default='Activo')
	socio_inscripcion = models.DateField(null=True, blank=True)

	def __str__(self):
		return str(self.socio_nombre)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True,blank=True)
    sector = models.ForeignKey('Sector',null=True, on_delete = models.CASCADE)
    pertenece_compania = models.BooleanField(default=True, blank=True)

    def __str__(self):
    	return self.user.username

    def get_name(self):
    	return self.user.first_name

    def get_lastname(self):
    	return self.user.last_name

    def get_fullname(self):
    	return self.user.first_name + " " + self.user.last_name

class Talonario(models.Model):
	talonario_recaudador = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, blank = True)
	talonario_estado = models.BooleanField(default = True)
	
	def __str__(self):
		return str(self.talonario_recaudador)

class Sector(models.Model):
	sector_nombre = models.CharField(max_length = 100, null = True, blank = True)

	def __str__(self):
		return str(self.sector_nombre)

	
		