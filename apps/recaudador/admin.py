# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.recaudador.models import *
# Register your models here.

@admin.register(Boleta)
class BoletaAdmin(admin.ModelAdmin):
	list_display = ('boleta_numero','boleta_fecha', 'boleta_socio', 'boleta_aporte','boleta_recaudador')
	search_fields = ['boleta_numero' , 'boleta_fecha', 'boleta_socio__socio_nombre', 'boleta_aporte', 'boleta_recaudador__user__username' ]

@admin.register(Socio)
class SocioAdmin(admin.ModelAdmin):
	list_display = ('id','socio_nombre', 'socio_direccion', 'socio_sector','socio_estado')
	search_fields = ['socio_nombre', 'socio_direccion', 'socio_sector','socio_estado']

@admin.register(Talonario)
class TalonarioAdmin(admin.ModelAdmin):
	list_display = ('id', 'talonario_estado',)
	search_fields = ['id',]

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	pass

@admin.register(Sector)
class SectorAdmin(admin.ModelAdmin):
	pass
