# -*- coding: utf-8 -*-
import datetime
# from __future__ import unicode_literals
from django.shortcuts import render
from apps.recaudador.models import *
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core import serializers
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from itertools import cycle


def group_check(user):
	try: 
		groups = Group.objects.get(user = user)
	except:
		pass

	if groups.name == 'Recaudador' or user.is_superuser and user.is_staff:
		return True
	else:
		return False


@login_required()
def inicio (request):
	context = {}
	if group_check(request.user):
		template_name = "recaudador/inicio.html"
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Recaudador!"
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def boletas(request):
	context = {}
	if group_check(request.user):
		template_name = "recaudador/anular_pago.html"
		context['sectores'] = Sector.objects.all()
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Recaudador!"
		return render(request, 'login/sin_permisos.html', context)

def buscar_recibo(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	socio_id = ddatos_string[0][0]
	ddatos_json = json.loads(socio_id)
	try:
		if Boleta.objects.filter(boleta_numero=int(ddatos_json['id_recibo'])).get(boleta_estado = True):
			data = serializers.serialize('json', Boleta.objects.filter(boleta_numero=int(ddatos_json['id_recibo'])))
			return HttpResponse(data)
	except:
		mensaje = 'Boleta esta anulada o no existe'
		return HttpResponse(JsonResponse({"mensaje":mensaje}), content_type='applicaction/json')

@login_required()
def anular_boleta(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	boleta_id = int(ddatos_json[0]['recibo'])
	for data in ddatos_json:
		if 'observacion' in data.keys():
			observacion = data['observacion']
	if Boleta.objects.filter(boleta_numero = boleta_id).get(boleta_estado = True):
		Boleta.objects.filter(boleta_numero = boleta_id).update(boleta_estado = False)
		Boleta.objects.filter(boleta_numero = boleta_id).update(boleta_socio_observacion = observacion)
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	else:
		return HttpResponse(JsonResponse({"respuesta":"Ups! Ocurrio un Error"}), content_type='applicaction/json')

@login_required()
def ingresar_socio(request):
	context = {}
	if group_check(request.user):
		template_name = "recaudador/ingresar_socio.html"
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Recaudador!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_socio(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	nuevo_socio = Socio()
	x = datetime.datetime.now()
	if x.day < 10 and x.month < 10:
		fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + "0" + str(x.day)
	elif x.day < 10 and x.month >= 10:
		fecha_actual = str(x.year) + "-" + str(x.month) + "-" + "0" + str(x.day)
	elif x.day >= 10 and x.month >= 10:
		fecha_actual = str(x.year) + "-" + str(x.month) + "-" + str(x.day)
	elif x.day >= 10 and x.month < 10:
		fecha_actual = str(x.year) + "-" + "0" + str(x.month) + "-" + str(x.day)
	for data in ddatos_json:
		if 'Nombre' in data.keys():
			nombre = data['Nombre']
		if 'Apellido' in data.keys():
			apellido = data['Apellido']
		if 'Rut' in data.keys():
			rut = data['Rut']
		if 'Direccion' in data.keys():
			direccion = data['Direccion']
		if 'Sector' in data.keys():
			sector = data['Sector']
		if 'Fecha_nac' in data.keys():
			fecha_nac = data['Fecha_nac']
		if 'Telefono' in data.keys():
			telefono = data['Telefono']
		if 'FechaPago' in data.keys():
			fechapago = data['FechaPago']
		if 'Monto' in data.keys():
			monto = data['Monto']
	if validarRut(rut):
		nombre_completo = nombre + " " + apellido
		if Socio.objects.filter(socio_nombre = nombre_completo):
			return HttpResponse(JsonResponse({"incorrecto":"Usuario ya existe!"}), content_type='applicaction/json')
		else:
			nuevo_socio.socio_nombre = nombre_completo
			nuevo_socio.socio_rut = rut
			nuevo_socio.socio_direccion = direccion
			nuevo_socio.socio_sector = Sector.objects.get(id = int(sector))
			nuevo_socio.socio_fecha_nacimiento = fecha_nac
			nuevo_socio.socio_telefono = telefono
			nuevo_socio.socio_aporte = monto
			nuevo_socio.socio_dia_pago = fechapago
			nuevo_socio.socio_estado = 'Activo'
			nuevo_socio.socio_inscripcion = fecha_actual
			nuevo_socio.save()
			return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	else:
		return HttpResponse(JsonResponse({"respuesta":"Rut Invalido!"}), content_type='applicaction/json')

@login_required()
def realizar_pago(request):
	context = {}
	if group_check(request.user):
		template_name = "recaudador/realizar_pago.html"
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id)
		context['recaudadores'] = recaudadores
		context['sectores'] = Sector.objects.all()
		return render(request,template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de Recaudador!"
		return render(request, 'login/sin_permisos.html', context)

def guardar_pago(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	n_recaudador = 0
	for data in ddatos_json:
		if 'Talonario' in data.keys():
			talonario = data['Talonario']
		if 'Boleta' in data.keys():
			boleta = data['Boleta']
		if 'N_Recuadador' in data.keys():
			n_recaudador = int(data['N_Recuadador'])
		if 'socio_id' in data.keys():
			id_socio = data['socio_id']
		if 'N_Socio' in data.keys():
			socio = data['N_Socio']
		if 'Direccion' in data.keys():
			direccion = data['Direccion']
		if 'sector' in data.keys():
			sector = data['sector']
		if 'FechaPago' in data.keys():
			pago = data['FechaPago']
		if 'meses' in data.keys():
			cantidad_mes = data['meses']
		if 'Monto' in data.keys():
			monto = data['Monto']
	if Boleta.objects.filter(boleta_numero = boleta):
		return HttpResponse(JsonResponse({"respuesta":"Boleta Ya existe!"}), content_type='applicaction/json')
	else:
		user = User.objects.get(id = n_recaudador)
		profile = Profile.objects.get(user = user.id)
		fecha_separada = pago.split('-')
		mes_actual = int(fecha_separada[1])
		año_actual = int(fecha_separada[0])
		for num_meses in range(int(cantidad_mes)):
			nueva_boleta = Boleta()
			nueva_boleta.boleta_recaudador = profile
			if mes_actual + num_meses == 13:
				mes_actual -= 12
				año_actual +=  1
				fecha_final = str(año_actual) + '-' + str(mes_actual + num_meses) + '-' + fecha_separada[2]
			else:
				fecha_final = str(año_actual) + '-' + str(mes_actual + num_meses) + '-' + fecha_separada[2]
			nueva_boleta.boleta_numero = int(boleta)
			if Talonario.objects.filter(id = talonario):
				nueva_boleta.talonario = talonario
			if Socio.objects.filter(id = id_socio).filter(socio_nombre = socio).filter(socio_direccion = direccion):
				nueva_boleta.boleta_socio = Socio.objects.filter(id = id_socio).filter(socio_nombre = socio).get(socio_direccion = direccion)
				nueva_boleta.boleta_socio_nombre = nueva_boleta.boleta_socio.socio_nombre
				nueva_boleta.boleta_socio_direccion = direccion
				nueva_boleta.boleta_socio_sector = Sector.objects.get(id = int(sector))
				nueva_boleta.boleta_fecha = fecha_final
				nueva_boleta.boleta_aporte = monto
				nueva_boleta.boleta_talonario = Talonario.objects.get(id=talonario)
				nueva_boleta.boleta_estado = True
				nueva_boleta.save()
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')

def buscar_IDsocio(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	socio_id = ddatos_string[0][0]
	ddatos_json = json.loads(socio_id)
	data = serializers.serialize('json', Socio.objects.filter(id=int(ddatos_json['socio_id'])))
	return HttpResponse(data)

def buscar_Dirsocio(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	socio_direccion = ddatos_string[0][0]
	ddatos_json = json.loads(socio_direccion)
	data = serializers.serialize('json', Socio.objects.filter(socio_direccion=ddatos_json['socio_direccion']))
	return HttpResponse(data)

def buscar_Nombresocio(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	socio_nombre = ddatos_string[0][0]
	ddatos_json = json.loads(socio_nombre)
	data = serializers.serialize('json', Socio.objects.filter(socio_nombre=ddatos_json['socio_nombre']))
	return HttpResponse(data)

def graficar(request):
	perfil = Profile.objects.get(user = request.user.id)
	boletas = Boleta.objects.filter(boleta_recaudador = perfil.id).filter(boleta_estado = True).order_by('boleta_fecha')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	datos = {}

	for boleta in boletas:
		if boleta.boleta_aporte != None and boleta.boleta_fecha != None:
			fecha_boleta = str(boleta.boleta_fecha)
			fecha_separada_boleta = fecha_boleta.split('-')
			if año == fecha_separada_boleta[0]:
				if int(fecha_separada_boleta[1]) > int(mes) - 6 and int(fecha_separada_boleta[1]) <= int(mes):
					if fecha_separada_boleta[1] not in datos.keys():
						datos[fecha_separada_boleta[1]] = boleta.boleta_aporte
					elif fecha_separada_boleta[1] in datos.keys():
						datos[fecha_separada_boleta[1]] += boleta.boleta_aporte
	return JsonResponse(datos)

def validarRut(rut):
	rut = rut.upper();
	rut = rut.replace("-","")
	# rut = rut.replace(".","")
	aux = rut[:-1]
	dv = rut[-1:]

	revertido = map(int, reversed(str(aux)))
	factors = cycle(range(2,8))
	s = sum(d * f for d, f in zip(revertido,factors))
	res = (-s)%11
	if str(res) == dv:
		return True
	elif dv=="K" and res==10:
		return True
	else:
		return False

def validar_talonario(request):
	datos = request.POST
	id = request.POST['id']
	recaudador = request.POST['recaudador']
	perfil = Profile.objects.get(user = recaudador)
	if Talonario.objects.filter(id = id).filter(talonario_recaudador = perfil.id):
		mensaje = 'OK'
	else:
		mensaje = "Talonario No Existe o No corresponde al Recaudador"
	return HttpResponse(JsonResponse({'respuesta':mensaje}), content_type='applicaction/json')

def validar_boleta(request):
	datos = request.POST
	id = request.POST['id']
	if Boleta.objects.filter(boleta_numero = id):
		mensaje = 'Boleta Ya existente!'
	else:
		mensaje = "OK"
	return HttpResponse(JsonResponse({'respuesta':mensaje}), content_type='applicaction/json')



