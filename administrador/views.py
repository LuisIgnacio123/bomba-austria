# -*- coding: utf-8 -*-
import datetime
# from __future__ import unicode_literals
from django.shortcuts import render
from apps.recaudador.models import *
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.core import serializers
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required, permission_required
from apps.recaudador.models import *


# Create your views here.

@login_required()
def administrador(request):
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		template_name = "inicio.html"
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
#crud de usuarios
def administrador_visualizar_usuarios(request):
	template_name = "visualizar_usuarios.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['usuarios'] = User.objects.all()
		# context['grupos'] = Group.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		# context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def administrador_eliminar_usuarios(request,id_usuario):
	usuario = User.objects.get(id=id_usuario)
	if Profile.objects.filter(user = usuario.id):
		profile = Profile.objects.get(user = usuario.id)
		if profile.delete():
			usuario.delete()
			return HttpResponseRedirect('/administrador/visualizar_usuarios/')

@login_required()
def administrador_modificar_usuarios(request,id_usuario):
	template_name = "crud_usuario/modificar_usuario.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['usuarios'] = User.objects.get(id = id_usuario)
		context['grupos'] = Group.objects.get(user = context['usuarios'])
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		# context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def administrador_agregar_usuario(request):
	template_name = "crud_usuario/agregar_usuario.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		# context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

def actualizar_usuario(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	print (ddatos_json)
	for data in ddatos_json:
		if 'id' in data.keys():
			id = data['id']
		if 'Nombre_user' in data.keys():
			nombre_usuario = data['Nombre_user']
		if 'Nombre' in data.keys():
			primer_nombre = data['Nombre']
		if 'Apellido' in data.keys():
			apellido = data['Apellido']
		if 'correo' in data.keys():
			correo = data['correo']
		if 'password' in data.keys():
			password = data['password']
		if 'password2' in data.keys():
			pass
		if 'es_staff' in data.keys():
			es_staff = data['es_staff']
		if 'is_superuser' in data.keys():
			super_user = data['is_superuser']
		if 'es_recaudador' in data.keys():
			es_recaudador = data['es_recaudador']
		if 'es_secretario' in data.keys():
			es_secretario = data['es_secretario']
		if 'es_tesorero' in data.keys():
			es_tesorero = data['es_tesorero']
		if 'es_admin' in data.keys():
			es_admin = data['es_admin']
		if 'estado' in data.keys():
			estado = data['estado']
	User.objects.filter(id=id).update(username = nombre_usuario)
	User.objects.filter(id=id).update(first_name = primer_nombre)
	User.objects.filter(id=id).update(last_name = apellido)
	User.objects.filter(id=id).update(email = correo)
	User.objects.get(id=id).set_password(password)
	User.objects.filter(id=id).update(is_staff = es_staff)
	User.objects.filter(id=id).update(is_superuser = super_user)
	User.objects.filter(id=id).update(is_active = estado)
	if es_recaudador == 'True':
		usuario = User.objects.get(id=id)
		usuario.groups.clear()
		grupo = Group.objects.get(name = 'Recaudador')
		usuario.groups.add(grupo)
	if es_secretario == 'True':
		usuario = User.objects.get(id=id)
		usuario.groups.clear()
		grupo = Group.objects.get(name = 'Secretario')
		usuario.groups.add(grupo)
	if es_tesorero == 'True':
		usuario = User.objects.get(id=id)
		usuario.groups.clear()
		grupo = Group.objects.get(name = 'Tesorero')
		usuario.groups.add(grupo)
	if es_admin == 'True':
		usuario = User.objects.get(id=id)
		usuario.groups.clear()
		grupo = Group.objects.get(name = 'administrador')
		usuario.groups.add(grupo)

	return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')

def agregar_usuario(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'id' in data.keys():
			id = data['id']
		if 'Nombre_user' in data.keys():
			nombre_usuario = data['Nombre_user']
		if 'Nombre' in data.keys():
			primer_nombre = data['Nombre']
		if 'Apellido' in data.keys():
			apellido = data['Apellido']
		if 'correo' in data.keys():
			correo = data['correo']
		if 'password' in data.keys():
			password = data['password']
		if 'password2' in data.keys():
			pass
		if 'sector' in data.keys():
			sector = data['sector']
		if 'es_staff' in data.keys():
			es_staff = data['es_staff']
		if 'is_superuser' in data.keys():
			super_user = data['is_superuser']
		if 'es_recaudador' in data.keys():
			es_recaudador = data['es_recaudador']
		if 'es_secretario' in data.keys():
			es_secretario = data['es_secretario']
		if 'es_tesorero' in data.keys():
			es_tesorero = data['es_tesorero']
		if 'es_admin' in data.keys():
			es_admin = data['es_admin']
		if 'pertenece' in data.keys():
			pertenece = data['pertenece']

	if not User.objects.filter(username = nombre_usuario):
		new_user = User.objects.create_user(
			username = nombre_usuario,
			first_name = primer_nombre,
			last_name = apellido,
			email = correo,
			password = password)
		new_user.is_active = True
		new_user.is_staff = es_staff
		new_user.is_superuser = super_user
		new_user.save()
		if es_recaudador == 'True':
			grupo = Group.objects.get(name = 'Recaudador')
			new_user.groups.add(grupo)
		if es_secretario == 'True':
			grupo = Group.objects.get(name = 'Secretario')
			new_user.groups.add(grupo)
		if es_tesorero == 'True':
			grupo = Group.objects.get(name = 'Tesorero')
			new_user.groups.add(grupo)
		nuevo_perfil = Profile()
		nuevo_perfil.user = new_user
		nuevo_perfil.sector = Sector.objects.get(id = int(sector))
		nuevo_perfil.pertenece_compania = pertenece
		nuevo_perfil.save()
		
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')
	else:
		usuario_existe = "Nombre usuario ya existe"
		return HttpResponse(JsonResponse({"respuesta":usuario_existe}), content_type='applicaction/json')


#crud de boleta
@login_required()
def administrador_visualizar_boletas(request):
	template_name = "visualizar_boletas.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['boletas'] = Boleta.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def administrador_eliminar_boleta(request,id_boleta):
	boleta = Boleta.objects.get(id = id_boleta)
	if boleta.delete():
		return HttpResponseRedirect('/administrador/visualizar_boletas/')

@login_required()
def administrador_modificar_boleta(request,id_boleta):
	template_name = 'crud_boleta/modificar_boleta.html'
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context = {}
		context['boleta'] = Boleta.objects.get(id = id_boleta)
		fecha = str(context['boleta'].boleta_fecha)
		fecha_separada = fecha.split('-')
		context['fecha'] = fecha_separada[0] + "-" + fecha_separada[1] + "-" + fecha_separada[2]
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id)
		context['recaudadores'] = recaudadores
		context['socios'] = Socio.objects.all().order_by('socio_nombre')
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

def validar_boleta(request):
	id = request.POST['id']
	if Boleta.objects.filter(boleta_numero = id):
		mensaje = "Boleta Existente"
	else:
		mensaje = 'OK'

	return HttpResponse(JsonResponse({'respuesta' : mensaje}), content_type='applicaction/json')

def validar_talonario(request):
	id = request.POST['id']
	if Talonario.objects.filter(id = id):
		mensaje = 'OK'
	else:
		mensaje = "Talonario No Existe"

	return HttpResponse(JsonResponse({'respuesta' : mensaje}), content_type='applicaction/json')

login_required()
def adminsitrador_agregar_boleta(request):
	template_name = 'crud_boleta/agregar_boleta.html'
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id).filter(is_active = True)
		context['recaudadores'] = recaudadores
		context['socios'] = Socio.objects.filter(socio_estado = 'Activo')
		context['talonarios'] = Talonario.objects.filter(talonario_estado = True)
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

def guardar_boleta(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	n_recaudador = 0
	for data in ddatos_json:
		if 'Talonario' in data.keys():
			talonario = data['Talonario']
		if 'Boleta' in data.keys():
			boleta = data['Boleta']
		if 'N_Recuadador' in data.keys():
			n_recaudador = int(data['N_Recuadador'])
		if 'socio_id' in data.keys():
			id_socio = data['socio_id']
		if 'N_Socio' in data.keys():
			socio = data['N_Socio']
		if 'Direccion' in data.keys():
			direccion = data['Direccion']
		if 'sector' in data.keys():
			sector = data['sector']
		if 'FechaPago' in data.keys():
			pago = data['FechaPago']
		if 'meses' in data.keys():
			cantidad_mes = data['meses']
		if 'Monto' in data.keys():
			monto = data['Monto']
	if Boleta.objects.filter(boleta_numero = boleta):
		return HttpResponse(JsonResponse({"respuesta":"Boleta Ya existe!"}), content_type='applicaction/json')
	else:
		user = User.objects.get(id = n_recaudador)
		profile = Profile.objects.get(user = user.id)
		fecha_separada = pago.split('-')
		mes_actual = int(fecha_separada[1])
		año_actual = int(fecha_separada[0])
		for num_meses in range(int(cantidad_mes)):
			nueva_boleta = Boleta()
			nueva_boleta.boleta_recaudador = profile
			if mes_actual + num_meses == 13:
				mes_actual -= 12
				año_actual +=  1
				fecha_final = str(año_actual) + '-' + str(mes_actual + num_meses) + '-' + fecha_separada[2]
			else:
				fecha_final = str(año_actual) + '-' + str(mes_actual + num_meses) + '-' + fecha_separada[2]
			nueva_boleta.boleta_numero = int(boleta)
			if Talonario.objects.filter(id = talonario):
				nueva_boleta.talonario = talonario
			if Socio.objects.filter(id = id_socio).filter(socio_nombre = socio).filter(socio_direccion = direccion):
				nueva_boleta.boleta_socio = Socio.objects.filter(id = id_socio).filter(socio_nombre = socio).get(socio_direccion = direccion)
				nueva_boleta.boleta_socio_nombre = nueva_boleta.boleta_socio.socio_nombre
				nueva_boleta.boleta_socio_direccion = direccion
				nueva_boleta.boleta_socio_sector = Sector.objects.get(id = int(sector))
				nueva_boleta.boleta_fecha = fecha_final
				nueva_boleta.boleta_aporte = monto
				nueva_boleta.boleta_talonario = Talonario.objects.get(id=talonario)
				nueva_boleta.boleta_estado = True
				nueva_boleta.save()
		return HttpResponse(JsonResponse({"respuesta":"correcto"}), content_type='applicaction/json')

#crud de talonarios
@login_required()
def administrador_visualizar_talonarios(request):
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		template_name = "visualizar_talonarios.html"
		context['talonarios'] = Talonario.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def administrador_agregar_talonario(request):
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		template_name = "crud_talonarios/agregar_talonario.html"
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id)
		context['recaudadores'] = recaudadores
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

@login_required()
def administrador_eliminar_talonario(request,id_talonario):
	talonario = Talonario.objects.get(id = id_talonario)
	if talonario.delete():
		return HttpResponseRedirect('/administrador/visualizar_talonarios/')


@login_required()
def administrador_modificar_talonario(request, id_talonario):
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		template_name = "crud_talonarios/modificar_talonario.html"
		context['talonario'] = Talonario.objects.get(id = id_talonario)
		grupo = Group.objects.get(name = 'Recaudador')
		recaudadores = User.objects.filter(groups = grupo.id)
		context['recaudadores'] = recaudadores
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

def actualizar_talonario(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'recaudador' in data.keys():
			recaudador = data['recaudador']
		if 'Talonario' in data.keys():
			id_talonario = data['Talonario']
		if 'estado' in data.keys():
			estado = data['estado']
	usuario = User.objects.get(id = recaudador)
	profile = Profile.objects.get(user = usuario.id)
	Talonario.objects.filter(id = id_talonario).update(talonario_recaudador = profile.id)
	Talonario.objects.filter(id =id_talonario).update(talonario_estado = estado)

	return HttpResponse(JsonResponse({'respuesta':'correcto'}), content_type='applicaction/json')

def guardar_talonario(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'recaudador' in data.keys():
			recaudador = data['recaudador']
		if 'Talonario' in data.keys():
			id_talonario = data['Talonario']
		if 'estado' in data.keys():
			estado = data['estado']
	nuevo_talonario = Talonario()
	nuevo_talonario.id = id_talonario
	usuario = User.objects.get(id = recaudador)
	profile = Profile.objects.get(user = usuario.id)
	nuevo_talonario.talonario_recaudador = profile
	nuevo_talonario.estado = estado
	nuevo_talonario.save()

	return HttpResponse(JsonResponse({'respuesta':'correcto'}), content_type='applicaction/json')

#crud de socios
@login_required()
def administrador_visualizar_socios(request):
	template_name = "visualizar_socios.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['socios'] = Socio.objects.all().order_by('socio_sector__sector_nombre')
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

	return render(request, template_name, context)

@login_required()
def administrador_agregar_socios(request):
	template_name = "crud_socio/nuevo_socio.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

	return render(request, template_name, context)

@login_required()
def administrador_modificar_socios(request, id_socio):
	template_name = "crud_socio/modificar_socio.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		data = Socio.objects.get(id = id_socio)
		fecha = data.socio_fecha_nacimiento
		context['socio'] = data
		context['fecha'] = fecha
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

	return render(request, template_name, context)

@login_required()
def administrador_eliminar_socios(request,id_socio):
	socio = Socio.objects.get(id=id_socio)
	if socio.delete():
		return HttpResponseRedirect('/administrador/visualizar_socios/')

#metodo que descargar un excel por penpyxl
def descargar_excel_socios(request):
	from openpyxl.writer.excel import save_virtual_workbook
	from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font
	import openpyxl.utils

	sector = Sector.objects.get(id = request.POST.get('sector'))
	socios = Socio.objects.filter(socio_sector = request.POST.get('sector'))
	print (socios)
	wb = openpyxl.Workbook()
	ws = wb.active
	ws.title = "Listado de Socios "
	filas = []
	filas = [
		'ID Socio',
		'Nombre Socio',
		'Socio Rut',
		'Direccion Socio',
		'Fecha de Nacimiento',
		'Telefono',
		'Aporte',
		'Dia de Pago',
		'Fecha de Inscripcion',
	]
	ws.append(filas)
	agregados = []
	for socio in socios:
		if socio not in agregados:
			print (socio)
			filas = [
				socio.id,
				socio.socio_nombre,
				socio.socio_rut,
				socio.socio_direccion,
				socio.socio_fecha_nacimiento,
				socio.socio_telefono,
				socio.socio_aporte,
				socio.socio_dia_pago,
				socio.socio_inscripcion,
			]
			agregados.append(socio)
			ws.append(filas)

	response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.ms-excel')
	response['Content-Disposition'] = 'inline;filename=' + "Listado_Socios.xlsx"
	return response

#CRUD sector
@login_required()
def administrador_visualizar_sector(request):
	template_name = "visualizar_sector.html"
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['sectores'] = Sector.objects.all()
		return render(request, template_name, context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)

	return render(request, template_name, context)

def administrador_agregar_sector(request):
	template_name = 'crud_sector/agregar_sector.html'
	if request.user.is_staff and request.user.is_superuser:
		return render(request,template_name)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)


def administrador_modificar_sector(request, id_sector):
	template_name = 'crud_sector/modificar_sector.html'
	context = {}
	if request.user.is_staff and request.user.is_superuser:
		context['sector'] = Sector.objects.get(id = id_sector)
		return render(request,template_name,context)
	else:
		context['messages'] =  "Usted no tiene permisos de  Administrador!"
		context['grupo'] =groups
		return render(request, 'login/sin_permisos.html', context)


def administrador_eliminar_sector(request, id_sector):
	sector = Sector.objects.get(id = id_sector)
	if sector.delete():
		return HttpResponseRedirect('/administrador/visualizar_sectores/')

def actualizar_sector(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'id_sector' in data.keys():
			id_sector = data['id_sector']
		if 'nombre_sector' in data.keys():
			nombre_sector = data['nombre_sector']
	try: 		
		Sector.objects.filter(id = id_sector).update(sector_nombre =  nombre_sector)
		return HttpResponse(JsonResponse({'respuesta': 'correcto'}))
	except:
		return HttpResponse(JsonResponse({'incorrecto': 'Talonario no existe'}))

def guardar_sector(request):
	datos = request.POST
	ddatos_string = list(datos.items())
	ddatos_json = json.loads(ddatos_string[0][0])
	for data in ddatos_json:
		if 'n_sector' in data.keys():
			nombre_sector = data['n_sector']

	if nombre_sector == '':
		return HttpResponse(JsonResponse({'incorrecto': 'Tiene que rellenar los campos!'}))
	elif nombre_sector != '':
		try:
			if Sector.objects.filter(sector_nombre = nombre_sector):
				return HttpResponse(JsonResponse({'incorrecto': 'Sector ya existe!'}))
			else:
				sector = Sector()
				sector.sector_nombre = nombre_sector
				sector.save()
				return HttpResponse(JsonResponse({'respuesta':'correcto'}))
		except:
			return HttpResponse(JsonResponse({'incorrecto': 'Ocurrio un error! Comuniquese con el administrador'}))

	

def graficar_boletas(request):
	boletas = Boleta.objects.filter(boleta_estado = True).order_by('boleta_fecha')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	datos = {}
	
	for boleta in boletas:
		if boleta.boleta_aporte != None and boleta.boleta_fecha != None:
			fecha_boleta = str(boleta.boleta_fecha)
			fecha_separada_boleta = fecha_boleta.split('-')
			if año == fecha_separada_boleta[0]:
				if fecha_separada_boleta[1] not in datos.keys():
					datos[fecha_separada_boleta[1]] = boleta.boleta_aporte
				elif fecha_separada_boleta[1] in datos.keys():
					datos[fecha_separada_boleta[1]] += boleta.boleta_aporte
	return JsonResponse(datos)

def graficar_usuarios(request):
	socios = Socio.objects.order_by('id')
	fecha_actual = datetime.datetime.now()
	fecha_separada = str(fecha_actual).split('-')
	año = fecha_separada[0]
	mes = fecha_separada[1]
	dia = fecha_separada[2]
	datos = {}
	
	for socio in socios:
		if socio.socio_inscripcion != None:
			fecha_inscripcion = str(socio.socio_inscripcion)
			fecha_separada_incripcion = fecha_inscripcion.split('-')
			if fecha_separada_incripcion[0] not in datos.keys():
				datos[fecha_separada_incripcion[0]] = 1
			elif fecha_separada_incripcion[0] in datos.keys():
				datos[fecha_separada_incripcion[0]] += 1
	return JsonResponse(datos)