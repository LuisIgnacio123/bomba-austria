from django.urls import path
from administrador import views
app_name = "administrador"

urlpatterns = [
	path('', views.administrador, name='administrador'),
	#CRUD socios
	path('visualizar_socios/', views.administrador_visualizar_socios, name='visualizar_socios'),
	path('agregar_socio/', views.administrador_agregar_socios, name='agregar_socio'),
	path('eliminar_socio/<int:id_socio>/', views.administrador_eliminar_socios, name='eliminar_socio'),
	path('modificar_socio/<int:id_socio>/', views.administrador_modificar_socios, name='modificar_socio'),
	path('descargar_excel_socios/', views.descargar_excel_socios, name='descargar_excel_socios'),



	#CRUD boletas
	path('visualizar_boletas/', views.administrador_visualizar_boletas, name='visualizar_boletas'),
	path('agregar_boleta/', views.adminsitrador_agregar_boleta, name='agregar_boleta'),
	path('eliminar_boleta/<int:id_boleta>/', views.administrador_eliminar_boleta, name='eliminar_boleta'),
	path('modificar_boleta/<int:id_boleta>/', views.administrador_modificar_boleta, name='modificar_boleta'),
	path('validar_boleta/', views.validar_boleta, name='validar_boleta'),
	path('validar_talonario/', views.validar_talonario, name='validar_talonario'),
	path('guardar_boleta/', views.guardar_boleta, name='guardar_boleta'),

	#CRUD usuarios
	path('visualizar_usuarios/', views.administrador_visualizar_usuarios, name='visualizar_usuarios'),
	path('modificar_ususario/<int:id_usuario>/', views.administrador_modificar_usuarios, name='modificar_ususario'),
	path('actualizar_usuario/', views.actualizar_usuario, name='actualizar_usuario'),
	path('agregar_nuevo_usuario/', views.administrador_agregar_usuario, name='agregar_nuevo_usuario'),
	path('agregar_usuario/', views.agregar_usuario, name='agregar_usuario'),
	path('eliminar_usuario/<int:id_usuario>/', views.administrador_eliminar_usuarios, name='eliminar_usuario'),
	
	#CRUD talonario
	path('visualizar_talonarios/', views.administrador_visualizar_talonarios, name='visualizar_talonarios'),
	path('modificar_talonario/<int:id_talonario>/', views.administrador_modificar_talonario, name='modificar_talonario'),
	path('anular_talonario/<int:id_talonario>/', views.administrador_eliminar_talonario, name='anular_talonario'),
	path('agregar_talonario/', views.administrador_agregar_talonario, name='agregar_talonario'),
	path('actualizar_talonario/', views.actualizar_talonario, name='actualizar_talonario'),
	path('guardar_talonario/', views.guardar_talonario, name='guardar_talonario'),
	path('eliminar_talonario/<int:id_talonario>/', views.administrador_eliminar_talonario, name='eliminar_talonario'),

	#CRUD sector
	path('visualizar_sectores/', views.administrador_visualizar_sector, name='visualizar_sectores'),
	path('agregar_sector/', views.administrador_agregar_sector, name='agregar_sector'),
	path('eliminar_sector/<int:id_sector>/', views.administrador_eliminar_sector, name='eliminar_sector'),
	path('modificar_sector/<int:id_sector>', views.administrador_modificar_sector, name='modificar_sector'),
	path('actualizar_sector/', views.actualizar_sector, name='actualizar_sector'),
	path('guardar_sector/', views.guardar_sector, name='guardar_sector'),
	
	

	path('graficar_boletas/', views.graficar_boletas, name='graficar_boletas'),
	path('graficar_usuarios/', views.graficar_usuarios, name='graficar_usuarios'),
]